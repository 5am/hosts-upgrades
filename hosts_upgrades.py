#! /usr/bin/env python3

# hosts_upgrades.py
#
# Connect to and run commands on selected local and remote hosts. 
# See hosts.json for selected hosts.
# This script assumes the details for selected hosts are already defined in 
# /etc/hosts and ~/.ssh/config.
#
# Author: Sam Howell
# Email: mail@samhowell.uk

__author__ = "Sam Howell"
__version__ = "0.1"

import sys
import subprocess
import json
import argparse
from rich import reconfigure, print
from fabric import Connection


def main():
    # Disable rich.print automatic bolding/highlighting.
    reconfigure(highlight=False)

    parser = argparse.ArgumentParser()
    parser.add_argument("--version", "-v",
                        help="print module name and version",
                        action="version",
                        version=f"%(prog)s {__version__}")
    # parser.add_argument("--log", "-l",
    #                     help="Write output to log in working directory.",
    #                     action="store_true")
    args = parser.parse_args()

    # Failed hosts will be added and printed before exit
    failed = []

    try:
        args
    except argparse.ArgumentError:
        parser.print_help()
        sys.exit(1)

    def upgrade():
        """Check if root user, run apt commands as current user or root."""
        try:
            user = c.run("whoami", hide="stdout").stdout.strip()
        except Exception:
            failed.append(j)
            print("Failed to connect or get current user.")
        else:
            if user == "root":
                try:
                    c.run("apt-get update", hide="stdout")
                    c.run("apt-get upgrade")
                    c.run("apt-get autoremove")
                except Exception:
                    failed.append(j)
                    print("Command(s) aborted or failed.")
            else:
                try:
                    c.sudo("apt-get update", hide="stdout")
                    c.sudo("apt-get upgrade")
                    c.sudo("apt-get autoremove")
                except Exception:
                    failed.append(j)
                    print("Command(s) aborted or failed.")

    # Open hosts file and get host types
    hosts_path = "./hosts.json"
    try:
        with open(hosts_path) as hosts_file:
            hosts = json.load(hosts_file)
    except FileNotFoundError:
        print(f"Couldn't find the hosts file at {hosts_path}")
        sys.exit(1)

    # Add SSH key for remote host connections
    subprocess.run("ssh-add")

    for i in hosts:
        print(f"\n[bold]Checking {i} hosts...")
        # Connect to then run commands on each host in turn
        for j in hosts[i]:
            c = Connection(j, connect_timeout=5)
            print(f"\n[bold]--> {j}:")
            upgrade()
            c.close()

    if len(failed):
        print(f"\nUpgrades aborted or failed for the following hosts:\n")
        for i in failed:
            print(f"[bold red]  {i}")
        print()

    sys.exit(0)


if __name__ == "__main__":
    main()
